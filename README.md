# RecommenderService

Movie recommendation service.

Ratings are stored in the Postgres database. Service uses it to periodically create user's preferences model. If movie was already rated service returns value from the database, if not - preference model is used to predict rating.


## Appendix 

### Running

You need to download and install sbt for this application to run.

Once you have sbt installed, the following at the command prompt will start up Play in development mode:

```
sbt run
```

Play will start up on the HTTP port at http://localhost:9000/.
### Usage:

Getting predicted rating of the movie for the user:

```
GET /v1/ratings/movies/{movieId}/users/{userId}

RESPONSE:

{ 
  "movieId":77,
  "userId":44,
  "rating":4.56,
  "isPredicted":true
}
```

Saving rating of the movie given by the user:

```
POST /v1/ratings/movies/{movieId}/users/{userId}

BODY:

{ "rating" : 4.5 }
```
### Error codes:

- 500 - no model available

- 404 - no data about the movie or the user available

### Deployment
Service is deployed here:

[https://movie-recommender-service.herokuapp.com/](https://movie-recommender-service.herokuapp.com/)