name := "RecommenderService"

version := "1.0"

lazy val `recommenderservice` = (project in file(".")).enablePlugins(PlayScala)

scalaVersion := "2.11.7"

fork in test := true
envVars in Test := Map("DATABASE_URL" -> "jdbc:postgresql://localhost:5432/test_movieratings")

libraryDependencies += guice

// https://mvnrepository.com/artifact/org.apache.spark/spark-mllib_2.11
libraryDependencies += "org.apache.spark" %% "spark-mllib" % "2.1.0"

libraryDependencies ++= List(
  "com.typesafe.slick" %% "slick" % "3.0.3",
  jdbc,
  "org.postgresql" % "postgresql" % "9.4-1201-jdbc41",
  ws,
  "org.slf4j" % "slf4j-nop" % "1.6.4",
  "org.xerial" % "sqlite-jdbc" % "3.7.15-M1"
)

libraryDependencies ++= Seq(
  "org.scalatestplus.play" %% "scalatestplus-play" % "3.1.1" % "test"
)
libraryDependencies += "org.mockito" % "mockito-core" % "2.10.0" % "test"

libraryDependencies ++= Seq(jdbc, cacheApi, ws, specs2 % Test)

unmanagedResourceDirectories in Test <+= baseDirectory(_ / "target/web/public/test")

resolvers += "scalaz-bintray" at "https://dl.bintray.com/scalaz/releases"

dependencyOverrides ++= Set(
  "com.fasterxml.jackson.core" % "jackson-databind" % "2.6.5"
)


