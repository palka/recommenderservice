package spark

import javax.inject._

import org.apache.spark.{SparkConf, SparkContext}
import play.api.inject.ApplicationLifecycle

import scala.concurrent.Future


/**
  * Created by Paulina Sadowska on 01.08.2017.
  */
@Singleton
class SparkContextWrapper @Inject()(lifecycle: ApplicationLifecycle) {

  val sparkConfig = new SparkConf()
    .setAppName("RecommenderService")
    .setMaster("local[*]")
    .set("spark.executor.memory", "3g")
    //.set("spark.driver.port", scala.util.Properties.envOrElse("PORT", "4567"))
    .set("spark.driver.allowMultipleContexts", "true")
  val sparkContext = new SparkContext(sparkConfig)

  lifecycle.addStopHook { () =>
    Future.successful(
      sparkContext.stop()
    )
  }
}
