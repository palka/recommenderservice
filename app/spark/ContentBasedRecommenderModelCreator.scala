package spark

import javax.inject.Inject
import org.apache.spark.mllib.tree.RandomForest
import resource.ContentBasedRecommenderModel
import spark.dataloader.DbMovieInfoDataLoader

import scala.concurrent.ExecutionContext

/**
  * Created by Paulina Sadowska on 18.04.2018.
  */
class ContentBasedRecommenderModelCreator @Inject()(sparkContextWrapper: SparkContextWrapper,
                                                    recommenderModelCreator: ContentBasedRecommenderModel,
                                                    dataLoader: DbMovieInfoDataLoader)(implicit ec: ExecutionContext) {

  private val maxDepth = scala.util.Properties.envOrElse("MAX_DEPTH", "10").toInt
  private val maxBins = scala.util.Properties.envOrElse("MAX_BINS", "80").toInt
  private val impurity = scala.util.Properties.envOrElse("IMPURITY", "variance")
  private val numTrees = scala.util.Properties.envOrElse("NUM_TREES", "5").toInt
  private val featureSubsetStrategy = scala.util.Properties.envOrElse("FEATURE_SUBSET_STRATEGY", "auto")

  private val categoricalFeaturesInfo = Map(
    1 -> 2, //adult
    //genres
    13 -> 2,
    14 -> 2,
    15 -> 2,
    16 -> 2,
    17 -> 2,
    18 -> 2,
    19 -> 2,
    20 -> 2,
    21 -> 2,
    22 -> 2,
    23 -> 2,
    24 -> 2,
    25 -> 2,
    26 -> 2,
    27 -> 2,
    28 -> 2,
    29 -> 2,
    30 -> 2,
    31 -> 2
  )

  def updateModel(): Unit = {
    val sparkContext = sparkContextWrapper.sparkContext
    val moviesInfo = dataLoader.loadMoviesInfo(sparkContext)
    val modelRandomRegression = RandomForest.trainRegressor(moviesInfo, categoricalFeaturesInfo,
      numTrees, featureSubsetStrategy, impurity, maxDepth, maxBins)
    recommenderModelCreator.set(modelRandomRegression)
  }
}