package spark

import javax.inject.Inject

import org.apache.spark.mllib.recommendation.ALS
import resource.CollaborativeRecommenderModel
import spark.dataloader.DbRatingsDataLoader

import scala.concurrent.ExecutionContext

/**
  * Created by Paulina Sadowska on 01.08.2017.
  */
class CollaborativeRecommenderModelCreator @Inject()(sparkContextWrapper: SparkContextWrapper,
                                                     recommenderModel: CollaborativeRecommenderModel,
                                                     dataLoader: DbRatingsDataLoader)(implicit ec: ExecutionContext) {

  private val rank = scala.util.Properties.envOrElse("RANK", "10").toInt
  private val lambda = scala.util.Properties.envOrElse("LAMBDA", "0.08").toDouble
  private val numIterations = scala.util.Properties.envOrElse("NUM_ITERATIONS", "5").toInt

  def updateModel(): Unit = {
    val sparkContext = sparkContextWrapper.sparkContext
    val ratings = dataLoader.loadRatings(sparkContext)
    val model = ALS.train(ratings, rank, numIterations, lambda)
    recommenderModel.set(model)
  }
}
