package spark.dataloader

import org.apache.spark.SparkContext
import org.apache.spark.mllib.recommendation.Rating
import org.apache.spark.rdd.RDD

/**
  * Created by Paulina Sadowska on 03.08.2017.
  */
trait RatingsDataLoader {
  def loadRatings(sparkContext: SparkContext): RDD[Rating]
}
