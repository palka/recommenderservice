package spark.dataloader

import database.movies.{MovieInfoConverter, MovieInfoDatabaseController}
import database.ratings.RatingsDatabaseController
import javax.inject.Inject
import org.apache.spark.SparkContext
import org.apache.spark.mllib.regression.LabeledPoint
import org.apache.spark.rdd.RDD

/**
  * Created by Paulina Sadowska on 18.04.2018.
  */
class DbMovieInfoDataLoader @Inject()(movieInfoDbController: MovieInfoDatabaseController,
                                      ratingsDatabaseController: RatingsDatabaseController,
                                      converter: MovieInfoConverter) {

  def loadMoviesInfo(sparkContext: SparkContext): RDD[LabeledPoint] = {
    val ratings = ratingsDatabaseController.getAll
      .filter(rating => movieInfoDbController.getMovieEntryExists(rating._2))
      .map {
        rating => {
          val item = movieInfoDbController.getMovieById(rating._2).get
          LabeledPoint(rating._3, converter.toVector(rating._1, item))
        }
      }
    sparkContext.parallelize(ratings)
  }
}