package spark.dataloader

import java.io.File

import org.apache.spark.SparkContext
import org.apache.spark.mllib.recommendation.Rating
import org.apache.spark.rdd.RDD

/**
  * Created by Paulina Sadowska on 25.07.2017.
  */
class CsvRatingsDataLoader extends RatingsDataLoader {
  private val DATA_DIRECTORY = "data"
  private val RATINGS = "ratings.csv"
  private val SEPARATOR = ","

  override def loadRatings(sparkContext: SparkContext): RDD[Rating] = {
    val ratings = sparkContext.textFile(new File(DATA_DIRECTORY, RATINGS).toString).map {
      line =>
        val fields = line.split(SEPARATOR)
        Rating(fields(0).toInt, fields(1).toInt, fields(2).toDouble)
    }
    ratings
  }
}
