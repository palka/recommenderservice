package spark.dataloader

import database.ratings.RatingsDatabaseController
import javax.inject.Inject
import org.apache.spark.SparkContext
import org.apache.spark.mllib.recommendation.Rating
import org.apache.spark.rdd.RDD

/**
  * Created by Paulina Sadowska on 03.08.2017.
  */
class DbRatingsDataLoader @Inject()(dbController: RatingsDatabaseController) extends RatingsDataLoader {
  override def loadRatings(sparkContext: SparkContext): RDD[Rating] = {
    val ratings = dbController.getAll.map {
      item => Rating(
        item._1, //user
        item._2, //movie
        item._3) //rating
    }
    sparkContext.parallelize(ratings)
  }
}
