package exceptions

/**
  * Created by Paulina Sadowska on 06.09.2017.
  */
case class NoMovieException(private val message: String = "no data about the movie available")
  extends NoSuchElementException(message)