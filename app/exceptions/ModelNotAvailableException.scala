package exceptions

/**
  * Created by Paulina Sadowska on 06.09.2017.
  */
case class ModelNotAvailableException (private val message: String = "recommendation model not available",
                                  private val cause: Throwable = None.orNull)
  extends Exception(message, cause)
