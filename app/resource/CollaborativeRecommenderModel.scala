package resource

import javax.inject._

import org.apache.spark.mllib.recommendation.MatrixFactorizationModel

/**
  * Created by Paulina Sadowska on 13.09.2017.
  */
@Singleton
class CollaborativeRecommenderModel {
  @volatile private var savedModel: Option[MatrixFactorizationModel] = None

  def set(model: MatrixFactorizationModel) {
    savedModel = Some(model)
  }

  def get: Option[MatrixFactorizationModel] = savedModel

  def isAvailable: Boolean = savedModel.isDefined
}
