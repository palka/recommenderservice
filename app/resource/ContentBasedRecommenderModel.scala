package resource

import javax.inject.Singleton
import org.apache.spark.mllib.tree.model.RandomForestModel

/**
  * Created by Paulina Sadowska on 19.04.2018.
  */
@Singleton
class ContentBasedRecommenderModel {

  @volatile private var savedModel: Option[RandomForestModel] = None

  def set(model: RandomForestModel) {
    savedModel = Some(model)
  }

  def get: Option[RandomForestModel] = savedModel

  def isAvailable: Boolean = savedModel.isDefined
}
