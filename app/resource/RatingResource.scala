package resource

import play.api.libs.json.{JsValue, Json, Writes}

/**
  * Created by Paulina Sadowska on 24.07.2017.
  */
abstract class RatingResource {
  val userId: Int
  val movieId: Int
  val rating: Double
  val isPredicted: Boolean
}

object DefaultResourceValues {
  val DEFAULT_RATING = -1.0
}

case class EmptyRatingResource(movieId: Int, userId: Int)
  extends RatingResource {
  override val isPredicted = false
  override val rating = DefaultResourceValues.DEFAULT_RATING
}

case class PredictedRatingResource(movieId: Int, userId: Int, rating: Double)
  extends RatingResource {
  override val isPredicted = true
}

case class SavedRatingResource(movieId: Int, userId: Int, rating: Double)
  extends RatingResource {
  override val isPredicted = false
}

object RatingResource {

  /**
    * Mapping to write a PostResource out as a JSON value.
    */
  implicit val implicitWrites = new Writes[RatingResource] {
    def writes(rating: RatingResource): JsValue = {
      Json.obj(
        "userId" -> rating.userId,
        "movieId" -> rating.movieId,
        "rating" -> rating.rating,
        "isPredicted" -> rating.isPredicted
      )
    }
  }
}

