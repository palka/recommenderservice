package resource

import javax.inject._

import com.google.inject.Inject


/**
  * Created by Paulina Sadowska on 12.08.2017.
  */
@Singleton
class ModelState @Inject()(collaborativeRecommenderModel: CollaborativeRecommenderModel,
                           contentBasedRecommenderModel: ContentBasedRecommenderModel) {
  private var dataChanged = false

  def shouldUpdateModel: Boolean = {
    dataChanged || !collaborativeRecommenderModel.isAvailable || !contentBasedRecommenderModel.isAvailable
  }

  def onDataChanged() = {
    dataChanged = true
  }

  def onModelRecalculated() = {
    println("model recalculated")
    dataChanged = false
  }
}
