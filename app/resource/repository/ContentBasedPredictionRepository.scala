package resource.repository

import database.movies.{MovieInfoConverter, MovieInfoDatabaseController}
import database.ratings.RatingsDatabaseController
import javax.inject.Inject
import resource.ContentBasedRecommenderModel

/**
  * Created by Paulina Sadowska on 19.04.2018.
  */
class ContentBasedPredictionRepository @Inject()(contentRecommenderModel: ContentBasedRecommenderModel,
                                                 movieInfoDbController: MovieInfoDatabaseController,
                                                 ratingsDatabaseController: RatingsDatabaseController,
                                                 movieInfoConverter: MovieInfoConverter) {


  def predict(movieId: Int, userId: Int, averageMovieRating: Double): Double = {
    if (contentRecommenderModel.isAvailable && movieInfoDbController.getMovieEntryExists(movieId)) {
      val entry = movieInfoDbController.getMovieById(movieId)
      val vector = movieInfoConverter.toVector(userId, entry.get)
      println("PREDICTING")
      contentRecommenderModel.get.get.predict(vector)
    }
    else {
      averageMovieRating
    }
  }
}
