package resource.repository

import database.ratings.RatingsDatabaseController
import javax.inject.Inject
import resource.{RatingResource, SavedRatingResource}

import scala.concurrent.ExecutionContext

/**
  * Created by Paulina Sadowska on 25.07.2017.
  */
class SavedRatingsRepository @Inject()(databaseRepository: RatingsDatabaseController)
                                      (implicit ec: ExecutionContext) extends RatingRepository {
  override def get(movieId: Int, userId: Int): Option[RatingResource] = {
    val savedRating = databaseRepository.get(movieId, userId)
    if (savedRating.isEmpty)
      return None
    Some(SavedRatingResource(movieId = movieId, userId = userId, rating = savedRating.get))
  }

  def save(movieId: Int, userId: Int, rating: Double): Int = {
    databaseRepository.save(movieId, userId, rating)
  }
}
