package resource.repository

import resource.RatingResource

/**
  * Created by Paulina Sadowska on 01.10.2017.
  */
trait WritableRatingRepository extends RatingRepository {
  def get(movieId: Int, userId: Int): Option[RatingResource]

  def save(movieId: Int, userId: Int, rating: Double): Int
}
