package resource.repository

import javax.inject.Inject

import resource.{EmptyRatingResource, ModelState, RatingResource}

import scala.concurrent.ExecutionContext

/**
  * Created by Paulina Sadowska on 25.07.2017.
  */
class MainRatingRepository @Inject()(savedRatingsRepository: SavedRatingsRepository,
                                     predictionRepository: PredictionRepository,
                                     modelState: ModelState)
                                    (implicit ec: ExecutionContext) extends WritableRatingRepository {

  override def get(movieId: Int, userId: Int): Option[RatingResource] = {
    val savedRating = savedRatingsRepository.get(movieId, userId)
    if (savedRating.isDefined) {
      return savedRating
    }
    val prediction = predictionRepository.get(movieId, userId)
    if (prediction.isDefined) {
      return prediction
    }
    Some(EmptyRatingResource(movieId = movieId, userId = userId))
  }

  override def save(movieId: Int, userId: Int, rating: Double): Int = {
    modelState.onDataChanged()
    savedRatingsRepository.save(movieId, userId, rating)
  }

}
