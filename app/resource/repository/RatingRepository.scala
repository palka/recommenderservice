package resource.repository

import resource.RatingResource

/**
  * Created by Paulina Sadowska on 25.07.2017.
  */
trait RatingRepository {
  def get(movieId: Int, userId: Int): Option[RatingResource]
}



