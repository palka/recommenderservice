package resource.repository

import database.ratings.RatingsDatabaseController
import exceptions.NoMovieException
import javax.inject.Inject
import resource.{CollaborativeRecommenderModel, PredictedRatingResource, RatingResource}

import scala.concurrent.ExecutionContext

/**
  * Created by Paulina Sadowska on 25.07.2017.
  */
class PredictionRepository @Inject()(collaborativeRecommenderModel: CollaborativeRecommenderModel,
                                     contentBasedPredictionRepository: ContentBasedPredictionRepository,
                                     databaseRepository: RatingsDatabaseController)
                                    (implicit ec: ExecutionContext) extends RatingRepository {

  private val MIN_USER_RATINGS: Int = scala.util.Properties.envOrElse("MIN_MOVIE_RATINGS", "1").toInt
  private val MIN_MOVIE_RATINGS: Int = scala.util.Properties.envOrElse("MIN_USER_RATINGS", "1").toInt

  override def get(movieId: Int, userId: Int): Option[RatingResource] = {
    val (movieRatingCount, averageMovieRating) = databaseRepository.getMovieRatingsInfo(movieId)
    if (movieRatingCount == 0) throw NoMovieException()

    val userRatingCount = databaseRepository.countUserRatings(userId)
    if (userRatingCount < MIN_USER_RATINGS | movieRatingCount < MIN_MOVIE_RATINGS | !collaborativeRecommenderModel.isAvailable) {
      val prediction = contentBasedPredictionRepository.predict(movieId, userId, averageMovieRating)
      println("AVERAGE " + averageMovieRating)
      println("PREDICTION " + prediction)
      return Some(PredictedRatingResource(userId = userId, movieId = movieId, rating = prediction))
    }
    val prediction = collaborativeRecommenderModel.get.get.predict(userId, movieId)
    Some(PredictedRatingResource(userId = userId, movieId = movieId, rating = prediction))
  }
}
