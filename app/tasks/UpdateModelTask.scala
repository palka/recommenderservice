package tasks

/**
  * Created by Paulina Sadowska on 30.07.2017.
  */

import javax.inject.Inject
import akka.actor.ActorSystem
import play.api.inject.{SimpleModule, _}
import resource.ModelState
import spark.{CollaborativeRecommenderModelCreator, ContentBasedRecommenderModelCreator}

import scala.concurrent.ExecutionContext
import scala.concurrent.duration._

class UpdateModelTask @Inject()(actorSystem: ActorSystem,
                                modelCreator: CollaborativeRecommenderModelCreator,
                                modelState: ModelState,
                                contentModelCreator: ContentBasedRecommenderModelCreator)
                               (implicit executionContext: ExecutionContext) {

  actorSystem.scheduler.schedule(
    initialDelay = 0.microseconds,
    interval = 30.seconds
  ) {
    if (modelState.shouldUpdateModel && scala.util.Properties.envOrElse("UPDATE_MODEL_ENABLED", "true") == "true") {
      println("UPDATE MODEL!")
      try {
        modelCreator.updateModel()
        contentModelCreator.updateModel()
        modelState.onModelRecalculated()
      } catch {
        case e: Exception => println("Model not recalculated, Reason: " + e);
      }
    }
    else {
      println("data hasn't changed or model recalculation disabled - model was not recalculated")
    }
  }
}

class UpdateModelModule extends SimpleModule(bind[UpdateModelTask].toSelf.eagerly())