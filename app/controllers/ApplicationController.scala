package controllers

import com.google.inject.Inject
import exceptions.ModelNotAvailableException
import play.api.libs.json.{JsValue, Json}
import play.api.mvc._
import resource.repository.MainRatingRepository

import scala.concurrent.ExecutionContext

class ApplicationController @Inject()(cc: ControllerComponents, repository: MainRatingRepository)(implicit ec: ExecutionContext) extends AbstractController(cc) {

  def index = Action { request =>
    Ok(s"Application is ready")
  }

  def predict(movieId: Int, userId: Int) = {
    Action { implicit request =>
      try {
        Ok(Json.toJson(repository.get(movieId, userId).get))
      }
      catch {
        case e: NoSuchElementException => NotFound(e.getMessage)
        case e: ModelNotAvailableException => InternalServerError(e.getMessage)
      }
    }

  }

  def saveRating(movieId: Int, userId: Int) = Action {
    request =>
      val body: AnyContent = request.body
      val jsonBody: Option[JsValue] = body.asJson
      jsonBody.map {
        json =>
          val rating = (json \ "rating").as[Double]
          val count = repository.save(movieId, userId, rating)
          Ok(s"$count row(s) updated")
      }.getOrElse {
        BadRequest("Expecting application/json request body")
      }
  }
}