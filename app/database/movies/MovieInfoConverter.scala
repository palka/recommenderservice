package database.movies

import org.apache.spark.mllib.linalg.Vectors
import org.apache.spark.mllib.linalg.Vector

/**
  * Created by Paulina Sadowska on 19.04.2018.
  */
class MovieInfoConverter {

  def toVector(userId: Long,
               item: ((Long, Int, Int, Int, Int),
                 (Int, Int, Int, Int, Int),
                 (Int, Int, Int, Int, Int),
                 (Int, Int, Int, Int, Int),
                 (Int, Int, Int, Int, Int),
                 (Int, Int, Int, Int, Int),
                 (Int, Int))): Vector = {
    Vectors.dense(
      userId, //user
      item._1._2, item._1._3, item._1._4, item._1._5,
      item._2._1, item._2._2, item._2._3, item._2._4, item._2._5,
      item._3._1, item._3._2, item._3._3, item._3._4, item._3._5,
      item._4._1, item._4._2, item._4._3, item._4._4, item._4._5,
      item._5._1, item._5._2, item._5._3, item._5._4, item._5._5,
      item._6._1, item._6._2, item._6._3, item._6._4, item._6._5,
      item._7._1, item._7._2)
  }
}
