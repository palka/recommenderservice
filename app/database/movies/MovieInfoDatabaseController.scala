package database.movies

import database.BaseDatabaseController
import slick.lifted.TableQuery

import scala.concurrent.Await
import scala.concurrent.duration.Duration

/**
  * Created by Paulina Sadowska on 18.04.2018.
  */
class MovieInfoDatabaseController extends BaseDatabaseController {
  private val dbActions = new MovieInfoDatabaseActions(TableQuery[MovieInfoTable])

  def getMovieById(movieId: Long): Option[((Long, Int, Int, Int, Int),
    (Int, Int, Int, Int, Int),
    (Int, Int, Int, Int, Int),
    (Int, Int, Int, Int, Int),
    (Int, Int, Int, Int, Int),
    (Int, Int, Int, Int, Int),
    (Int, Int))] = {
    val db = openDb
    var savedMovie: Option[((Long, Int, Int, Int, Int),
      (Int, Int, Int, Int, Int),
      (Int, Int, Int, Int, Int),
      (Int, Int, Int, Int, Int),
      (Int, Int, Int, Int, Int),
      (Int, Int, Int, Int, Int),
      (Int, Int))] = None
    databaseScope(db) {
      val savedMovies = Await.result(db.run(dbActions.getMovieById(movieId)), Duration.Inf).toList
      if (savedMovies.nonEmpty) {
        savedMovie = Some(savedMovies.head)
      }
    }
    savedMovie
  }

  def getMovieEntryExists(movieId: Long): Boolean = {
    val db = openDb
    var movieEntryExists = false
    databaseScope(db) {
      movieEntryExists = Await.result(db.run(dbActions.getMovieEntryExists(movieId)), Duration.Inf)
    }
    movieEntryExists
  }
}
