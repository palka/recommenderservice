package database.movies

import slick.lifted.TableQuery
import slick.driver.H2Driver.api._

/**
  * Created by Paulina Sadowska on 18.04.2018.
  */
class MovieInfoDatabaseActions(moviesTable: TableQuery[MovieInfoTable]) {

  def getMovieById(movieId: Long): DBIO[Seq[((Long, Int, Int, Int, Int),
    (Int, Int, Int, Int, Int),
    (Int, Int, Int, Int, Int),
    (Int, Int, Int, Int, Int),
    (Int, Int, Int, Int, Int),
    (Int, Int, Int, Int, Int),
    (Int, Int))]] = {
    moviesTable.filter(_.movieId === movieId).result
  }

  def getMovieEntryExists(movieId: Long): DBIO[Boolean] = {
    moviesTable.filter(_.movieId === movieId).exists.result
  }
}
