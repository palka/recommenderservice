package database.movies

import slick.driver.H2Driver.api._
import slick.lifted.ProvenShape

/**
  * Created by Paulina Sadowska on 17.04.2018.
  */
class MovieInfoTable(tag: Tag)
  extends Table[((Long, Int, Int, Int, Int),
    (Int, Int, Int, Int, Int),
    (Int, Int, Int, Int, Int),
    (Int, Int, Int, Int, Int),
    (Int, Int, Int, Int, Int),
    (Int, Int, Int, Int, Int),
    (Int, Int))](tag, "movie_recommendation_info") {

  def movieId: Rep[Long] = column[Long]("movie_id")

  def adult: Rep[Int] = column[Int]("adult")

  def budget: Rep[Int] = column[Int]("budget")

  def originalLanguage: Rep[Int] = column[Int]("original_language")

  def popularity: Rep[Int] = column[Int]("popularity")


  def revenue: Rep[Int] = column[Int]("revenue")

  def runtime: Rep[Int] = column[Int]("runtime")

  def releaseYear: Rep[Int] = column[Int]("release_year")

  def voteAverage: Rep[Int] = column[Int]("vote_average")

  def voteCount: Rep[Int] = column[Int]("vote_count")


  def directorId: Rep[Int] = column[Int]("director_id")

  def firstActorId: Rep[Int] = column[Int]("first_actor_id")

  def secondActorId: Rep[Int] = column[Int]("second_actor_id")

  def genreAction: Rep[Int] = column[Int]("genre_action")

  def genreAdventure: Rep[Int] = column[Int]("genre_adventure")


  def genreAnimation: Rep[Int] = column[Int]("genre_animation")

  def genreComedy: Rep[Int] = column[Int]("genre_comedy")

  def genreCrime: Rep[Int] = column[Int]("genre_crime")

  def genreDocumentary: Rep[Int] = column[Int]("genre_documentary")

  def genreDrama: Rep[Int] = column[Int]("genre_drama")


  def genreFamily: Rep[Int] = column[Int]("genre_family")

  def genreFantasy: Rep[Int] = column[Int]("genre_fantasy")

  def genreHistory: Rep[Int] = column[Int]("genre_history")

  def genreHorror: Rep[Int] = column[Int]("genre_horror")

  def genreMusic: Rep[Int] = column[Int]("genre_music")


  def genreMystery: Rep[Int] = column[Int]("genre_mystery")

  def genreRomance: Rep[Int] = column[Int]("genre_romance")

  def genreScienceFiction: Rep[Int] = column[Int]("genre_science_fiction")

  def genreTvMovie: Rep[Int] = column[Int]("genre_tv_movie")

  def genreThriller: Rep[Int] = column[Int]("genre_thriller")


  def genreWar: Rep[Int] = column[Int]("genre_war")

  def genreWestern: Rep[Int] = column[Int]("genre_western")

  // Every table needs a * projection with the same type as the table's type parameter
  def * : ProvenShape[((Long, Int, Int, Int, Int), (Int, Int, Int, Int, Int), (Int, Int, Int, Int, Int), (Int, Int, Int,
    Int, Int), (Int, Int, Int, Int, Int), (Int, Int, Int, Int, Int), (Int, Int))] =

    ((movieId, adult, budget, originalLanguage, popularity),
      (revenue, runtime, releaseYear, voteAverage, voteCount),
      (directorId, firstActorId, secondActorId, genreAction, genreAdventure),
      (genreAnimation, genreComedy, genreCrime, genreDocumentary, genreDrama),
      (genreFamily, genreFantasy, genreHistory, genreHorror, genreMusic),
      (genreMystery, genreRomance, genreScienceFiction, genreTvMovie, genreThriller),
      (genreWar, genreWestern))
}
