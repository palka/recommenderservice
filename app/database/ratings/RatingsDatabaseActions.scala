package database.ratings

import slick.driver.H2Driver.api._
import slick.lifted.TableQuery


/**
  * Created by Paulina Sadowska on 29.07.2017.
  */
class RatingsDatabaseActions(ratingsTable: TableQuery[RatingsTable]) {

  def getById(movieId: Rep[Int], userId: Rep[Int]): DBIO[Seq[(Int, Int, Double)]] = {
    ratingsTable.filter(_.movieId === movieId).filter(_.userId === userId).result
  }

  def getMovieById(movieId: Rep[Int]): DBIO[Seq[(Int, Int, Double)]] = {
    ratingsTable.filter(_.movieId === movieId).result
  }

  def getUserById(userId: Rep[Int]): DBIO[Seq[(Int, Int, Double)]] = {
    ratingsTable.filter(_.userId === userId).result
  }

  def insert(movieId: Int, userId: Int, rating: Double): DBIO[Int] = {
    ratingsTable += (userId, movieId, rating)
  }

  def delete(movieId: Int, userId: Int): DBIO[Int] = {
    ratingsTable.filter(_.movieId === movieId).filter(_.userId === userId).delete
  }

  def getAll: DBIO[Seq[(Int, Int, Double)]] = {
    ratingsTable.result
  }
}

