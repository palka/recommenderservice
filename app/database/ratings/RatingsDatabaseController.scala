package database.ratings

import database.BaseDatabaseController
import slick.driver.H2Driver.api._
import slick.lifted.TableQuery

import scala.concurrent.Await
import scala.concurrent.duration.Duration

/**
  * Created by Paulina Sadowska on 29.07.2017.
  */
class RatingsDatabaseController extends BaseDatabaseController {
  private val ratingsDbActions = new RatingsDatabaseActions(TableQuery[RatingsTable])

  def get(movieId: Int, userId: Int): Option[Double] = {
    val db = openDb
    var savedRating: Option[Double] = None
    databaseScope(db) {
      val savedRatings = Await.result(db.run(ratingsDbActions.getById(movieId, userId)), Duration.Inf).toList
      if (savedRatings.nonEmpty)
        savedRating = Some(savedRatings.head._3)
    }
    savedRating
  }

  def getAll: List[(Int, Int, Double)] = {
    val db = openDb
    var savedRatings: List[(Int, Int, Double)] = List.empty
    databaseScope(db) {
      savedRatings = Await.result(db.run(ratingsDbActions.getAll), Duration.Inf).toList
    }
    savedRatings
  }

  def save(movieId: Int, userId: Int, rating: Double): Int = {
    val db = openDb
    var updatedRowsCount: Int = 0
    databaseScope(db) {
      updatedRowsCount = Await.result(db
        .run(ratingsDbActions.delete(movieId, userId)
          .andThen(ratingsDbActions.insert(movieId, userId, rating))
        ), Duration.Inf)
    }
    updatedRowsCount
  }

  def countUserRatings(userId: Int): Int = {
    val db = openDb
    var userRatingsCount: Int = 0
    databaseScope(db) {
      userRatingsCount = Await.result(db.run(ratingsDbActions.getUserById(userId)), Duration.Inf).size
    }
    userRatingsCount
  }

  def getMovieRatingsInfo(movieId: Int): (Int, Double) = {
    val db = openDb
    var ratingsCount: Int = 0
    var averageRating: Double = 0.0
    databaseScope(db) {
      val movies = Await.result(db.run(ratingsDbActions.getMovieById(movieId)), Duration.Inf)
      ratingsCount = movies.size
      averageRating = movies.map(_._3).sum / ratingsCount
    }
    (ratingsCount, averageRating)
  }
}
