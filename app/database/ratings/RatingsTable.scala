package database.ratings

/**
  * Created by Paulina Sadowska on 29.07.2017.
  */

import slick.driver.H2Driver.api._
import slick.lifted.ProvenShape


class RatingsTable(tag: Tag)
  extends Table[(Int, Int, Double)](tag, "user_rating") {

  def userId: Rep[Int] = column[Int]("user_id")

  def movieId: Rep[Int] = column[Int]("movie_id")

  def rating: Rep[Double] = column[Double]("rating")

  // Every table needs a * projection with the same type as the table's type parameter
  def * : ProvenShape[(Int, Int, Double)] =
    (userId, movieId, rating)
}
