package database

import slick.driver.H2Driver.api._

import scala.util.{Failure, Success, Try}

/**
  * Created by Paulina Sadowska on 18.04.2018.
  */
class BaseDatabaseController {

  protected def openDb: Database = {
    Database.forConfig("postgres")
  }

  protected def databaseScope[T](db: Database)(fn: => T): Unit = {
    Try {
      fn
    } match {
      case Success(x) => db.close()
      case Failure(e) =>
        println(e)
        db.close()
    }
  }
}
