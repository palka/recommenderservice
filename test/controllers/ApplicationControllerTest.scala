package controllers

import org.scalatest.mockito.MockitoSugar
import org.scalatestplus.play.PlaySpec
import org.scalatestplus.play.guice.GuiceOneServerPerSuite
import play.api.libs.json.Json
import play.api.libs.ws.WSClient

import scala.concurrent.Await
import scala.concurrent.duration.Duration

/**
  * Created by Paulina Sadowska on 01.10.2017.
  */
class ApplicationControllerTest extends PlaySpec
  with GuiceOneServerPerSuite
  with MockitoSugar {

  private val OK = 200
  private val NOT_FOUND = 404

  "ApplicationController#mainPage" must {
    "has proper config" in {
      System.getenv("DATABASE_URL") mustBe "jdbc:postgresql://localhost:5432/test_movieratings"
    }
    "return OK status" in {
      val wsClient = app.injector.instanceOf[WSClient]
      val myPublicAddress = s"localhost:$port"
      val testGatewayURL = s"http://$myPublicAddress"
      val callbackURL = s"http://$myPublicAddress/callback"
      val response = Await.result(wsClient.url(testGatewayURL).withQueryStringParameters("callbackURL" -> callbackURL).get(), Duration.Inf)

      response.status mustBe OK
      response.body mustEqual "Application is ready"
    }
  }


  "ApplicationController#get" must {
    "return OK status when data found" in {
      val wsClient = app.injector.instanceOf[WSClient]
      val myPublicAddress = s"localhost:$port/v1/ratings/movies/${Constants.MOVIE_ID}/users/${Constants.USER_ID}"
      val testGatewayURL = s"http://$myPublicAddress"
      val callbackURL = s"http://$myPublicAddress/callback"
      val response = Await.result(wsClient.url(testGatewayURL).withQueryStringParameters("callbackURL" -> callbackURL).get(), Duration.Inf)

      response.status mustBe OK
      /*   response.body mustEqual
           "{\"userId\":" + Constants.USER_ID +
             ",\"movieId\":" + Constants.MOVIE_ID +
             ",\"rating\":" + Constants.RATING +
             ",\"isPredicted\":" + Constants.IS_PREDICTED + "}"*/
      //problem with rating precision in test (5.1 -> 5.09999
    }

    "return NOT_FOUND when movie not found" in {
      val wsClient = app.injector.instanceOf[WSClient]
      val myPublicAddress = s"localhost:$port/v1/ratings/movies/374237/users/${Constants.USER_ID}"
      val testGatewayURL = s"http://$myPublicAddress"
      val callbackURL = s"http://$myPublicAddress/callback"
      val response = Await.result(wsClient.url(testGatewayURL).withQueryStringParameters("callbackURL" -> callbackURL).get(), Duration.Inf)
      response.status mustBe NOT_FOUND
    }
  }

  "ApplicationController#save" must {
    "return OK after saving rating" in {
      val wsClient = app.injector.instanceOf[WSClient]
      val myPublicAddress = s"localhost:$port/v1/ratings/movies/${Constants.MOVIE_ID}/users/${Constants.USER_ID}"
      val testGatewayURL = s"http://$myPublicAddress"
      val callbackURL = s"http://$myPublicAddress/callback"
      val data = Json.obj(
        "rating" -> Constants.RATING
      )
      val response = Await.result(
        wsClient.url(testGatewayURL)
          .withQueryStringParameters("callbackURL" -> callbackURL)
          .post(data),
        Duration.Inf)
      response.status mustBe OK
    }
  }
}
