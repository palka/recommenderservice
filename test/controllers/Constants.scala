package controllers

/**
  * Created by Paulina Sadowska on 01.10.2017.
  */
object Constants {
  val MOVIE_ID = 1026
  val USER_ID = 66
  val RATING = 4.7
  val IS_PREDICTED = false
}