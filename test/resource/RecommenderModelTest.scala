package resource

import org.apache.spark.mllib.recommendation.MatrixFactorizationModel
import org.scalatest.mockito.MockitoSugar
import org.scalatestplus.play.PlaySpec

/**
  * Created by Paulina Sadowska on 30.09.2017.
  */
class RecommenderModelTest extends PlaySpec with MockitoSugar {

  "RecommenderModel#isAvailable" should {
    "not be available initially" in {
      val recommenderModel = new CollaborativeRecommenderModel
      recommenderModel.isAvailable mustBe false
    }
    "be available when model set" in {
      val recommenderModel = new CollaborativeRecommenderModel
      recommenderModel.set(mock[MatrixFactorizationModel])
      recommenderModel.isAvailable mustBe true
    }
  }

  "RecommenderModel#get" should {
    "return previously set model" in {
      val recommenderModel = new CollaborativeRecommenderModel
      val model = mock[MatrixFactorizationModel]
      recommenderModel.set(model)
      recommenderModel.get.get mustEqual model
    }
    "return none when model not set" in {
      val recommenderModel = new CollaborativeRecommenderModel
      recommenderModel.get.isDefined mustBe false
    }
  }
}
