package resource

import org.mockito.Mockito._
import org.scalatest.mockito.MockitoSugar
import org.scalatestplus.play.PlaySpec


/**
  * Created by Paulina Sadowska on 29.09.2017.
  */
class ModelStateTest extends PlaySpec with MockitoSugar {

  "ModelState#shouldUpdateModel" should {
    "be true initially when model not available" in {
      val recommenderModel = mock[CollaborativeRecommenderModel]
      val contentRecommenderModel = mock[ContentBasedRecommenderModel]
      val modelState = new ModelState(recommenderModel, contentRecommenderModel)

      when(recommenderModel.isAvailable) thenReturn false
      when(contentRecommenderModel.isAvailable) thenReturn false

      modelState.shouldUpdateModel mustBe true
    }
    "be false initially when model available" in {
      val recommenderModel = mock[CollaborativeRecommenderModel]
      val contentRecommenderModel = mock[ContentBasedRecommenderModel]
      val modelState = new ModelState(recommenderModel, contentRecommenderModel)

      when(recommenderModel.isAvailable) thenReturn true
      when(contentRecommenderModel.isAvailable) thenReturn true

      modelState.shouldUpdateModel mustBe false
    }
    "be true initially when only one model available" in {
      val recommenderModel = mock[CollaborativeRecommenderModel]
      val contentRecommenderModel = mock[ContentBasedRecommenderModel]
      val modelState = new ModelState(recommenderModel, contentRecommenderModel)

      when(recommenderModel.isAvailable) thenReturn true
      when(contentRecommenderModel.isAvailable) thenReturn false

      modelState.shouldUpdateModel mustBe true
    }
    "be true when data changed" in {
      val recommenderModel = mock[CollaborativeRecommenderModel]
      val contentRecommenderModel = mock[ContentBasedRecommenderModel]
      val modelState = new ModelState(recommenderModel, contentRecommenderModel)

      modelState.onDataChanged()

      when(recommenderModel.isAvailable) thenReturn true
      when(contentRecommenderModel.isAvailable) thenReturn true

      modelState.shouldUpdateModel mustBe true

      when(recommenderModel.isAvailable) thenReturn false
      modelState.shouldUpdateModel mustBe true
    }

    "be false when model recalculated and available" in {
      val recommenderModel = mock[CollaborativeRecommenderModel]
      val contentRecommenderModel = mock[ContentBasedRecommenderModel]
      val modelState = new ModelState(recommenderModel, contentRecommenderModel)

      modelState.onDataChanged()
      modelState.onModelRecalculated()

      when(recommenderModel.isAvailable) thenReturn true
      when(contentRecommenderModel.isAvailable) thenReturn true

      modelState.shouldUpdateModel mustBe false
    }
  }
}
