package resource.repository

import database.ratings.RatingsDatabaseController
import org.mockito.Mockito._
import org.scalatest.mockito.MockitoSugar
import org.scalatestplus.play.PlaySpec

import scala.concurrent.ExecutionContext

/**
  * Created by Paulina Sadowska on 30.09.2017.
  */
class SavedRatingsRepositoryTest extends PlaySpec with MockitoSugar {

  implicit val executionContext = ExecutionContext.Implicits.global

  val USER_ID = 1
  var MOVIE_ID = 34
  val RATING = 4.7

  "SavedRatingsRepository#save" should {
    "save data in database repository" in {
      val database = mock[RatingsDatabaseController]
      val repository = new SavedRatingsRepository(database)
      repository.save(MOVIE_ID, USER_ID, RATING)
      verify(database).save(MOVIE_ID, USER_ID, RATING)
    }
  }

  "SavedRatingsRepository#get" should {
    "return saved rating when available" in {
      val database = mock[RatingsDatabaseController]
      val repository = new SavedRatingsRepository(database)
      when(database.get(MOVIE_ID, USER_ID)) thenReturn Some(RATING)
      repository.get(MOVIE_ID, USER_ID).isDefined mustBe true
      val result = repository.get(MOVIE_ID, USER_ID).get
      result.rating mustEqual RATING
      result.movieId mustEqual MOVIE_ID
      result.userId mustEqual USER_ID
    }
    "return None when rating not saved" in {
      val database = mock[RatingsDatabaseController]
      val repository = new SavedRatingsRepository(database)
      when(database.get(MOVIE_ID, USER_ID)) thenReturn None
      repository.get(MOVIE_ID, USER_ID) mustEqual None
    }
  }
}