package resource.repository

import org.scalatest.mockito.MockitoSugar
import org.scalatestplus.play.PlaySpec
import resource.{DefaultResourceValues, ModelState, PredictedRatingResource, SavedRatingResource}
import org.mockito.Mockito._

import scala.concurrent.ExecutionContext

/**
  * Created by Paulina Sadowska on 30.09.2017.
  */
class MainRatingRepositoryTest extends PlaySpec with MockitoSugar {

  implicit val executionContext = ExecutionContext.Implicits.global

  val USER_ID = 1
  var MOVIE_ID = 34
  val PREDICTED_RATING = 4.7
  val SAVED_RATING = 3.1

  "MainRatingRepository#save" should {
    "save rating and call onDataChanged in ModelState" in {
      val savedRepository = mock[SavedRatingsRepository]
      val predictionRepository = mock[PredictionRepository]
      val modelState = mock[ModelState]
      val repository = new MainRatingRepository(savedRepository, predictionRepository, modelState)
      repository.save(MOVIE_ID, USER_ID, SAVED_RATING)
      verify(savedRepository).save(MOVIE_ID, USER_ID, SAVED_RATING)
      verify(modelState).onDataChanged()
    }
  }

  "MainRatingRepository#get" should {
    val modelState = mock[ModelState]
    "return empty rating if saved and predicted not found" in {
      val savedRepository = mock[SavedRatingsRepository]
      val predictionRepository = mock[PredictionRepository]
      when(predictionRepository.get(MOVIE_ID, USER_ID)) thenReturn None
      when(savedRepository.get(MOVIE_ID, USER_ID)) thenReturn None
      val repository = new MainRatingRepository(savedRepository, predictionRepository, modelState)
      val result = repository.get(MOVIE_ID, USER_ID)
      result.isDefined mustBe true
      result.get.isPredicted mustBe false
      result.get.userId mustBe USER_ID
      result.get.movieId mustBe MOVIE_ID
      result.get.rating mustBe DefaultResourceValues.DEFAULT_RATING
    }
    "return predicted rating if saved not found" in {
      val savedRepository = mock[SavedRatingsRepository]
      val predictionRepository = mock[PredictionRepository]
      val expectedResource = PredictedRatingResource(userId = USER_ID, movieId = MOVIE_ID, rating = PREDICTED_RATING)
      when(predictionRepository.get(MOVIE_ID, USER_ID)) thenReturn Some(expectedResource)
      when(savedRepository.get(MOVIE_ID, USER_ID)) thenReturn None
      val repository = new MainRatingRepository(savedRepository, predictionRepository, modelState)
      val result = repository.get(MOVIE_ID, USER_ID)
      result.isDefined mustBe true
      result.get mustEqual expectedResource
    }
    "return saved rating if found" in {
      val savedRepository = mock[SavedRatingsRepository]
      val predictionRepository = mock[PredictionRepository]
      val expectedResource = SavedRatingResource(userId = USER_ID, movieId = MOVIE_ID, rating = SAVED_RATING)
      when(savedRepository.get(MOVIE_ID, USER_ID)) thenReturn None
      when(savedRepository.get(MOVIE_ID, USER_ID)) thenReturn Some(expectedResource)
      val repository = new MainRatingRepository(savedRepository, predictionRepository, modelState)
      val result = repository.get(MOVIE_ID, USER_ID)
      result.isDefined mustBe true
      result.get mustEqual expectedResource
    }
  }
}