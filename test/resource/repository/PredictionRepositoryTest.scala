package resource.repository

import database.ratings.RatingsDatabaseController
import exceptions.NoMovieException
import org.apache.spark.mllib.recommendation.MatrixFactorizationModel
import org.mockito.Mockito._
import org.scalatest.mockito.MockitoSugar
import org.scalatestplus.play.PlaySpec
import resource.CollaborativeRecommenderModel

import scala.concurrent.ExecutionContext

/**
  * Created by Paulina Sadowska on 30.09.2017.
  */
class PredictionRepositoryTest extends PlaySpec with MockitoSugar {

  implicit val executionContext = ExecutionContext.Implicits.global

  val USER_ID = 1
  var MOVIE_ID = 34
  val AVERAGE_RATING = 4.7
  val PREDICTED_RATING = 2.2

  "PredictionRepository#get" should {
    val model = new CollaborativeRecommenderModel
    val database = mock[RatingsDatabaseController]
    val contentBasedRepository = mock[ContentBasedPredictionRepository]
    val repository = new PredictionRepository(model, contentBasedRepository, database)
    "return average rating if no rating from the user found in database" in {
      when(database.countUserRatings(USER_ID)) thenReturn 0
      when(database.getMovieRatingsInfo(MOVIE_ID)) thenReturn ((6, AVERAGE_RATING))
      when(contentBasedRepository.predict(MOVIE_ID, USER_ID, AVERAGE_RATING)) thenReturn AVERAGE_RATING
      val result = repository.get(MOVIE_ID, USER_ID)
      result.isDefined mustBe true
      result.get.isPredicted mustBe true
      result.get.userId mustBe USER_ID
      result.get.movieId mustBe MOVIE_ID
      result.get.rating mustBe AVERAGE_RATING
    }
    "throw NoMovieException when no rating of the movie found in database" in {
      when(database.countUserRatings(USER_ID)) thenReturn 1
      when(database.getMovieRatingsInfo(MOVIE_ID)) thenReturn ((0, 0.6))
      try {
        repository.get(MOVIE_ID, USER_ID)
        fail
      } catch {
        case _: NoMovieException => //success
      }
    }
    "return average rating if user ratings count less than MIN_USER_COUNT" in {
      when(database.countUserRatings(USER_ID)) thenReturn 1
      when(database.getMovieRatingsInfo(MOVIE_ID)) thenReturn ((6, AVERAGE_RATING))
      when(contentBasedRepository.predict(MOVIE_ID, USER_ID, AVERAGE_RATING)) thenReturn AVERAGE_RATING
      val result = repository.get(MOVIE_ID, USER_ID)
      result.isDefined mustBe true
      result.get.isPredicted mustBe true
      result.get.userId mustBe USER_ID
      result.get.movieId mustBe MOVIE_ID
      result.get.rating mustBe AVERAGE_RATING
    }
    "return average rating if movie ratings count less than MIN_MOVIE_COUNT" in {
      when(database.countUserRatings(USER_ID)) thenReturn 77
      when(database.getMovieRatingsInfo(MOVIE_ID)) thenReturn ((1, AVERAGE_RATING))
      when(contentBasedRepository.predict(MOVIE_ID, USER_ID, AVERAGE_RATING)) thenReturn AVERAGE_RATING
      val result = repository.get(MOVIE_ID, USER_ID)
      result.isDefined mustBe true
      result.get.isPredicted mustBe true
      result.get.userId mustBe USER_ID
      result.get.movieId mustBe MOVIE_ID
      result.get.rating mustBe AVERAGE_RATING
    }
    "return average rating if model does not exist" in {
      when(database.countUserRatings(USER_ID)) thenReturn 77
      when(database.getMovieRatingsInfo(MOVIE_ID)) thenReturn ((99, AVERAGE_RATING))
      when(contentBasedRepository.predict(MOVIE_ID, USER_ID, AVERAGE_RATING)) thenReturn AVERAGE_RATING
      val result = repository.get(MOVIE_ID, USER_ID)
      result.isDefined mustBe true
      result.get.isPredicted mustBe true
      result.get.userId mustBe USER_ID
      result.get.movieId mustBe MOVIE_ID
      result.get.rating mustBe AVERAGE_RATING
    }
    "return predicted rating when model available and there is enough ratings" in {
      when(database.countUserRatings(USER_ID)) thenReturn 77
      when(database.getMovieRatingsInfo(MOVIE_ID)) thenReturn ((99, AVERAGE_RATING))
      when(contentBasedRepository.predict(MOVIE_ID, USER_ID, AVERAGE_RATING)) thenReturn AVERAGE_RATING
      val matrixModel = mock[MatrixFactorizationModel]
      when(matrixModel.predict(USER_ID, MOVIE_ID)) thenReturn PREDICTED_RATING
      model.set(matrixModel)
      val result = repository.get(MOVIE_ID, USER_ID)
      result.isDefined mustBe true
      result.get.isPredicted mustBe true
      result.get.userId mustBe USER_ID
      result.get.movieId mustBe MOVIE_ID
      result.get.rating mustBe PREDICTED_RATING
    }
  }
}